import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URLEncoder;
import java.net.URL;

import com.google.gson.*;


public class Wxhw
{
   public static String getWeather(String zip)
	{
	 JsonElement jse = null;
    String shortURL = null;
/*
Location:      Honolulu, HI
Time:          Last Updated on March 5, 2:00 PM HST
Weather:       Mostly Cloudy
Temperature F: 79.0
Wind:          From the SW at 1.3 MPH Gusting to 4.2 MPH
Pressure inHG: 29.95
*/  String newLine   = System.getProperty("line.separator");
    String city      = "";
    String state     = "";
    String temp      = "";
    String location  = "Location: " + city + ", " +state;
    String time      = "";
    String weather   = "";
    String tempature = "";
    String wind_s    = "";
    String pressure  = "";
    String total     = "";

		try
		{
			// Construct Bitly API URL
			URL zipURL = new URL("http://api.wunderground.com/api/517d32876b6ce4f7/geolookup/q/" + zip + ".json");


			// Open the URL
			InputStream is = zipURL.openStream(); // throws an IOException
			BufferedReader br = new BufferedReader(new InputStreamReader(is));

			// Read the result into a JSON Element
			jse = new JsonParser().parse(br);
      
			// Close the connection
			is.close();
			br.close();
		}
		catch (java.io.UnsupportedEncodingException uee)
		{
			uee.printStackTrace();
		}
		catch (java.net.MalformedURLException mue)
		{
			mue.printStackTrace();
		}
		catch (java.io.IOException ioe)
		{
			ioe.printStackTrace();
		}

		if (jse != null)
		{

         if( !jse.getAsJsonObject().get("response").getAsJsonObject().has("error"))        
         { 
            city = jse.getAsJsonObject().get("location").getAsJsonObject().get("city").getAsString();
            state = jse.getAsJsonObject().get("location").getAsJsonObject().get("state").getAsString();
         }
         else
         {
            System.out.println("Please enter a correct zip code!");
         }
      }

if (!jse.getAsJsonObject().get("response").getAsJsonObject().has("error"))
{
      //Second url for city / state...
      try
		{
			// Second URL for zip->city conversion
         URL cityURL = new URL("http://api.wunderground.com/api/517d32876b6ce4f7/conditions/q/" + state + "/" + city + ".json");

			// Open the URL
			InputStream is = cityURL.openStream(); // throws an IOException
			BufferedReader br = new BufferedReader(new InputStreamReader(is));

			// Read the result into a JSON Element
			jse = new JsonParser().parse(br);
      
			// Close the connection
			is.close();
			br.close();
		}
		catch (java.io.UnsupportedEncodingException uee)
		{
			uee.printStackTrace();
		}
		catch (java.net.MalformedURLException mue)
		{
			mue.printStackTrace();
		}
		catch (java.io.IOException ioe)
		{
			ioe.printStackTrace();
		}

      if (jse != null)
		{
         location  = "Location:       " + jse.getAsJsonObject().get("current_observation").getAsJsonObject().get("display_location").getAsJsonObject().get("full").getAsString();
         time      = "Time:           " + jse.getAsJsonObject().get("current_observation").getAsJsonObject().get("observation_time").getAsString();
         weather   = "Weather:        " + jse.getAsJsonObject().get("current_observation").getAsJsonObject().get("weather").getAsString();
         tempature = "Tempature F:    " + jse.getAsJsonObject().get("current_observation").getAsJsonObject().get("temp_f").getAsString();
         wind_s    = "Wind:           " + jse.getAsJsonObject().get("current_observation").getAsJsonObject().get("wind_string").getAsString();
         pressure  = "Pressure inHG:  " + jse.getAsJsonObject().get("current_observation").getAsJsonObject().get("pressure_mb").getAsString();
              
         total     = location + newLine + time + newLine + weather + newLine + tempature + newLine + wind_s + newLine + pressure;
      }
      
}

    return total;
	}

/*
Location:      Honolulu, HI
Time:          Last Updated on March 5, 2:00 PM HST
Weather:       Mostly Cloudy
Temperature F: 79.0
Wind:          From the SW at 1.3 MPH Gusting to 4.2 MPH
Pressure inHG: 29.95




 TextField displayWeather;//, urlshow; //txtinf
 // Button urlshorten, btnclear;//btncalc, btnclr
   
  
  @Override
  public void start(Stage primaryStage) 
  {
    // Make the controls
    displayWeather=new TextField(Wxhw.getWeather(Integer.toString(95765)));
   // urlshow = new TextField("Shortened URL");
   // urlshorten=new Button("Shorten"); //
   // btnclear=new Button("Clear"); //clear
    
    // Center text in label
    displayWeather.setAlignment(Pos.CENTER);
    //urlshow.setAlignment(Pos.CENTER);
    //urlshorten.setAlignment(Pos.CENTER);
    //btnclear.setAlignment(Pos.CENTER);
    
    // Apply ccs-like style to label
    displayWeather.setStyle("-fx-border-color: #000; -fx-padding: 5px;");
    // Make container for app
    HBox root = new HBox();
    // Put container in middle of scene
    root.setAlignment(Pos.CENTER);
    // Spacing between items
    root.setSpacing(15);
    // Add to HBox
    root.getChildren().add(displayWeather);
    /*root.getChildren().add(urlshow);
    root.getChildren().add(urlshorten);
    root.getChildren().add(btnclear);*/

 /*   // Set widths
    setWidths();
    //attach buttons to code in separate method
    attachCode();
    // Set the scene
    Scene scene = new Scene(root, 800, 150);
    primaryStage.setTitle("Brenton Kludt");
    primaryStage.setScene(scene);
    primaryStage.show();
  }
   
  public void setWidths()
    {
      // Set widths of all controls
      displayWeather.setMaxWidth(300);
     /* urlshow.setMaxWidth(200);
      urlshorten.setMaxWidth(100);
 /*     btnclear.setMaxWidth(100);*/
   /* }
    
  public void attachCode()
  {
    // Attach actions to buttons
   // displayWeather.setOnAction(e -> displayWeather(e));

    //btnclear.setOnAction(e -> btnclear(e));
  }
  
 /* public void urlshorten(ActionEvent e) //
  {
      urlshow.setText(getLink(urlenter.getText()));
  }
  
  
    public void btnclear(ActionEvent e)
  {
    urlenter.setText("http://");
    urlshow.setText("Shortened URL");
    urlshorten.requestFocus();

  }*/

  public static void main(String[] args) 
  {
    Wxhw B = new Wxhw();
    System.out.println(Wxhw.getWeather(args[0]));
  }




}